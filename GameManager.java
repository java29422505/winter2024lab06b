public class GameManager{

	
	private Deck drawPile;
	
	private Card centerCard;
	
	private Card playerCard;

	// this is the constractor of the this class
	public GameManager(){

		this.drawPile= new Deck();

		drawPile.shuffleDeck();// shuffles the deck.

		//initializing two card fields to draw a card form the pile cards. 

		this.centerCard = drawPile.drawTopCard();

		this.playerCard = drawPile.drawTopCard();
	}
	// to strings prints the the cards 

	public String toString(){

		return "\nCenter Card: "+ this.centerCard+ "\n"+ "Player Card: "+this.playerCard; 	
	}

	public void dealCards(){

		// shuffle the pile

		this.drawPile.shuffleDeck();

		// if the length of the deck of card is more then 2 or equal then it should update the player's hand card and the center card

		if (drawPile.length()>=2){

			centerCard= this.drawPile.drawTopCard();

			playerCard= this.drawPile.drawTopCard();
		}
	}
	// this shows the number of cards 

	public int getNumberOfCards(){

		return drawPile.length();
	}

	public int calculatePoints(){

		// if the value of the card in center and the value in player's card are the same then it returns 4

		if (this.centerCard.getValue() == this.playerCard.getValue()){

			return 4;
		}
		// if the suit of the card in center and the player's card are the same then it returns 2

		else if (this.centerCard.getSuit() == this.playerCard.getSuit()){

			return 2;
		}
		// otherwise if its niether then it will retrun -1
	
		else {return -1;
		}

	}
}