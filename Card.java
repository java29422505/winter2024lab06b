public class Card{
	private String suit;
	private String value;

	public Card(String suit, String value){

		if (suit == null || value == null){
			throw new IllegalArgumentException ("suit and value can not be null");
		}

		this.suit= suit;
		this.value= value;

	}

	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	public String toString(){


		String printCard = this.value+ " of "+this.suit +"\n";

		return printCard;
		

	}
}

